//
//  ExampleClass.swift
//  ExampleFramework
//
//  Created by karnakar on 08/11/17.
//  Copyright © 2017 MagikMinds. All rights reserved.
//

import UIKit

public class ExampleClass: NSObject {
    
    public override init() {
         print("iniitalized from project")
    }
    
    public func frameworkName() {
        print(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String)
    }
}
